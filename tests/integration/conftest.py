from shutil import copy

from os import path
import sys
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import pytest
from api import create_app
from constants import PATIENTS_DATABASE, PROJECT_ROOT


@pytest.fixture
def client(tmpdir):

    copy(f"{PROJECT_ROOT}/{PATIENTS_DATABASE}", tmpdir.dirpath())

    temp_db_file = f"sqlite:///{tmpdir.dirpath()}/{PATIENTS_DATABASE}"

    app = create_app(temp_db_file)
    app.config["TESTING"] = True

    with app.test_client() as client:
        yield client