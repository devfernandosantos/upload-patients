from resources.patients_resource import PATIENTS_ENDPOINT
import io, json


def test_patients_post(client):

    file_name = 'filetest.csv'
    with open('filetest.csv', 'rb') as input_file:            
        input_file_stream = io.BytesIO(input_file.read())
    data = {            
        'patients': (input_file_stream, file_name),            
        'file': file_name
    }
    
    response = client.post(f'{PATIENTS_ENDPOINT}', content_type='multipart/form-data', data=data)
    assert response.status_code == 201


def test_patients_post_error(client):

    file_name = 'filetest.csv'
    with open('filetest.csv', 'rb') as input_file:            
        input_file_stream = io.BytesIO(input_file.read())
    data = {            
        'patients': (input_file_stream, file_name),            
        'file': file_name
    }
    
    response = client.post(f'{PATIENTS_ENDPOINT}', content_type='content-type/json', data=data)
    assert response.status_code == 400


def test_get_all_patients(client):

    response = client.get(f'{PATIENTS_ENDPOINT}')
    assert response.status_code == 200


def test_get_all_patients_by_classification(client):

    response = client.get(f'{PATIENTS_ENDPOINT}?classification=ckd')

    for patient in response.json['results']:
        assert patient['classification'] == 'ckd'


def test_get_single_patient(client):

    request_patient = client.get('http://localhost:5000/api/patients')
    patient = json.loads(request_patient.data.decode('utf-8'))
    if patient['results']:
        public_id = '/' + patient['results'][0]['public_id']
    else:
        public_id = ''
    
    response = client.get(f'/api/patients{public_id}')

    assert response.status_code == 200


def test_get_single_patient_not_found(client):
    
    response = client.get('/api/patients/133771331')
    assert response.status_code == 404