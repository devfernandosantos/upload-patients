from database import db
import uuid


class Patient(db.Model):

    def generate_uuid():
        return str(uuid.uuid4())

    __tablename__ = 'patient'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(db.String(), default=generate_uuid)
    age = db.Column(db.Integer, nullable=True)
    bp = db.Column(db.Integer, nullable=True)
    sg = db.Column(db.Float, nullable=True)
    al = db.Column(db.Integer, nullable=True)
    su = db.Column(db.Integer, nullable=True)
    rbc = db.Column(db.String(), nullable=True)
    pc = db.Column(db.String(), nullable=True)
    pcc = db.Column(db.String(), nullable=True)
    ba = db.Column(db.String(), nullable=True)
    bgr = db.Column(db.Integer, nullable=True)
    bu = db.Column(db.Integer, nullable=True)
    sc = db.Column(db.Float, nullable=True)
    sod = db.Column(db.Integer, nullable=True)
    pot = db.Column(db.Float, nullable=True)
    hemo = db.Column(db.Float, nullable=True)
    pcv = db.Column(db.Integer, nullable=True)
    wc = db.Column(db.Integer, nullable=True)
    rc = db.Column(db.Float, nullable=True)
    htn = db.Column(db.String(), nullable=True)
    dm = db.Column(db.String(), nullable=True)
    cad = db.Column(db.String(), nullable=True)
    appet = db.Column(db.String(), nullable=True)
    pe = db.Column(db.String(), nullable=True)
    ane = db.Column(db.String(), nullable=True)
    classification = db.Column(db.String(), nullable=False)

    def __repr__(self):
        return(
            f'**Patient** '
            f'id: {self.id}'
            f'age: {self.age}'
            f'bp: {self.bp}'
            f'sg: {self.sg}'
            f'al: {self.al}'
            f'su: {self.su}'
            f'rbc: {self.rbc}'
            f'pc: {self.pc}'
            f'pcc: {self.pcc}'
            f'ba: {self.ba}'
            f'bgr: {self.bgr}'
            f'bu: {self.bu}'
            f'sc: {self.sc}'
            f'sod: {self.sod}'
            f'pot: {self.pot}'
            f'hemo: {self.hemo}'
            f'pcv: {self.pcv}'
            f'wc: {self.wc}'
            f'rc: {self.rc}'
            f'htn: {self.htn}'
            f'dm: {self.dm}'
            f'cad: {self.cad}'
            f'appet: {self.appet}'
            f'pe: {self.pe}'
            f'ane: {self.ane}'
            f'classification: {self.classification}'
            f'**Patient** '
        )
