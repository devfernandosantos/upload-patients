Note 1: 

I am not allowing the identifier to be 0, so you need to modify the sample.csv dataset and change the id of the first patient before submitting.


Note 2:

For security purposes I am not returning the identifier (primary key) in the json return, I created a public id so that a specific patient can be retrieved.


Instructions:


Create the virtual environment (virtualenv) and run the command:

pip install -r requirements.txt.


Run the tests using the command:

pytest


Run the system using the command:

python api.py


GET (Disable CSV file upload)
http://localhost:5000/api/patients


GET (Disable CSV file upload)
http://localhost:5000/api/patients/{public_id}


GET (Disable CSV file upload)
http://localhost:5000/api/patients?classification={classification}


POST (Multipart Form CSV)
http://localhost:5000/api/patients