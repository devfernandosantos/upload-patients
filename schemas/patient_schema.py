from marshmallow import Schema, fields, post_load, pre_dump
from models.patient import Patient


class PatientSchema(Schema):
    id = fields.Integer()
    public_id = fields.String(allow_none=True)
    age = fields.Integer(allow_none=True)
    bp = fields.Integer(allow_none=True)
    sg = fields.Float(allow_none=True)
    al = fields.Integer(allow_none=True)
    su = fields.Integer(allow_none=True)
    rbc = fields.String(allow_none=True)
    pc = fields.String(allow_none=True)
    pcc = fields.String(allow_none=True)
    ba = fields.String(allow_none=True)
    bgr = fields.Integer(allow_none=True)
    bu = fields.Integer(allow_none=True)
    sc = fields.Float(allow_none=True)
    sod = fields.Integer(allow_none=True)
    pot = fields.Float(allow_none=True)
    hemo = fields.Float(allow_none=True)
    pcv = fields.Integer(allow_none=True)
    wc = fields.Integer(allow_none=True)
    rc = fields.Float(allow_none=True)
    htn = fields.String(allow_none=True)
    dm = fields.String(allow_none=True)
    cad = fields.String(allow_none=True)
    appet = fields.String(allow_none=True)
    pe = fields.String(allow_none=True)
    ane = fields.String(allow_none=True)
    classification = fields.String(allow_none=False)

    @post_load
    def make_patient(self, data, **kwargs):
        return Patient(**data)

    class Meta:
        load_only = (
            'id',
        )
        ordered = True