import logging

from flask import request
from flask_restful import Resource, abort
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import desc

from database import db
from models.patient import Patient
from schemas.patient_schema import PatientSchema
import pandas as pd
import numpy as np
from sklearn.impute import SimpleImputer


PATIENTS_ENDPOINT = '/api/patients'
logger = logging.getLogger(__name__)


class PatientsResource(Resource):

    def get(self, id=None):
        if not id:
            classification = request.args.get('classification')
            logger.info(
                f'Retrieving all patients, optionally filtered by classification={classification}'
            )

            return self._get_all_patients(classification), 200
        
        logger.info(f'Retrieving patient by public_id {id}')

        try:
            return self._get_patient_by_id(id), 200
        except NoResultFound:
            abort(404, message="Patient not found")
    

    def _get_patient_by_id(self, id):
        
        patient = Patient.query.filter_by(public_id=id).first()
        patient_json = PatientSchema().dump(patient)

        if not patient_json:
            raise NoResultFound()

        logger.info(f"Patient retrieved from database {patient_json}")
        return patient_json

    
    def _get_paginated_json(self, patients_json, url, count, start, limit):

        start = int(start)
        limit = int(limit)

        data = {}
        data['count'] = count
        data['start'] = None if not start else start
        data['limit'] = None if not limit else limit
        
        if start == 1:
            data['previous'] = None
        else:
            start_pagination = max(1, start - limit)
            limit_pagination = start - 1
            data['previous'] = url + '?start=%d&limit=%d' % (start_pagination, limit_pagination)

        if start + limit > count:
            data['next'] = None
        else:
            start_pagination = start + limit
            data['next'] = url + '?start=%d&limit=%d' % (start_pagination, limit)

        data['results'] = patients_json[(start - 1):(start - 1 + limit)]

        return data
    

    def _get_all_patients(self, classification):

        if classification:
            patients = Patient.query.filter_by(classification=classification).all()
        else:
            patients = Patient.query.order_by(desc(Patient.id)).all()
        
        patients_json = [PatientSchema().dump(patient) for patient in patients]

        logger.info('Patients successfully retrieved')

        return self._get_paginated_json(
            patients_json, f'http://localhost:5000{PATIENTS_ENDPOINT}', len(patients_json), request.args.get('start', 1), request.args.get('limit', 20)
        )

    
    def create_object(self, key_list, values):
        
        patient_object = {}

        for index, value in enumerate(key_list):
            patient_object.update({value: values[index]})

        return patient_object


    def post(self):

        file = pd.read_csv(request.files['patients'])
        keys = list(file.columns[0:26])
        patients = file.iloc[:, 0:26].values

        imputer = SimpleImputer(missing_values=np.nan, strategy='constant', fill_value='0')
        imputer.fit(patients[:, 0:26])
        patients = imputer.transform(patients[:, 0:26])
        patients = patients.tolist()

        patients_list = []

        for patient in patients:
            patients_list.append(self.create_object(keys, patient))

        list_of_integers = ['wc', 'rc']
        list_of_strings = ['rbc', 'pc', 'pcc', 'ba', 'htn', 'dm', 'cad', 'appet', 'pe', 'ane']

        try:
            for patient in patients_list:
                
                if patient['id'] == 0:
                    return {'msg': 'Patient id cannot be 0'}, 400

                if db.session.query(db.session.query(Patient).filter_by(id=patient['id']).exists()).scalar():
                    return {'msg': f"Integrity Error, patient {patient['id']} is already in the database"}, 400

                for integer in list_of_integers:
                    patient[integer] = 0 if not isinstance(patient[integer], int) else patient[integer]
                
                for string in list_of_strings:
                    patient[string] = None if patient[string] == '0' else patient[string]

                patient = PatientSchema().load(patient)
                db.session.add(patient)
            db.session.commit()
        except Exception as e:
            logger.warning(
                f'Unexpected Error: {e}'
            )
            return {'msg': 'Unexpected error'}, 500
        else:
            return {'msg': 'Successful upload'}, 201