import logging
import sys
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from flask import Flask
from flask_restful import Api

from constants import PROJECT_ROOT, PATIENTS_DATABASE
from database import db
from resources.patients_resource import PatientsResource, PATIENTS_ENDPOINT


def create_app(db_location):
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        datefmt='%m-%d %H:%M',
        handlers=[logging.FileHandler('log'), logging.StreamHandler()],
    )

    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = db_location
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
    with app.app_context():
           db.create_all()

    api = Api(app)
    api.add_resource(PatientsResource, PATIENTS_ENDPOINT, f'{PATIENTS_ENDPOINT}/<id>')
    return app


if __name__ == '__main__':
    app = create_app(f'sqlite:////{PROJECT_ROOT}/{PATIENTS_DATABASE}')
    app.run(debug=True)
